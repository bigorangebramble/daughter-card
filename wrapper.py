#!/usr/bin/python
import paramiko
import sched
import time

s1 = paramiko.SSHClient()
s1.set_missing_host_key_policy(paramiko.AutoAddPolicy())
s1.connect('192.168.50.118', 22, 'pi', 'raspberry')

s2 = paramiko.SSHClient()
s2.set_missing_host_key_policy(paramiko.AutoAddPolicy())
s2.connect('192.168.50.119', 22, 'pi', 'raspberry')

def periodic(scheduler, interval, action, actionargs=()):
    scheduler.enter(interval, 1, periodic, (scheduler, interval, action, actionargs))
    action(*actionargs)

def do_it():
    command = 'python /home/pi/git/daughter-card/measure.py'
    i = 0
    j = 0

    print "*****************************"

    print "Node 1 Measurements:"
    (stdin, stdout, stderr) = s1.exec_command(command)
    for line in stdout.readlines():

        i = i + 1

        if i == 1:
            print "  Shunt Voltage:"
        elif i == 2:
            print "  Bus Voltage:"
        elif i == 3:
            print "  Current:"

        print "    " + line.rstrip('\n')

    print "Node 2 Measurements:"
    (stdin, stdout, stderr) = s2.exec_command(command)
    for line in stdout.readlines():

        j = j + 1

        if j == 1:
            print "  Shunt Voltage:"
        elif j == 2:
            print "  Bus Voltage:"
        elif j == 3:
            print "  Current:"

        print "    " + line.rstrip('\n')

scheduler = sched.scheduler(time.time, time.sleep)
periodic(scheduler, 1, do_it)
scheduler.run()

s1.close()
s2.close()
