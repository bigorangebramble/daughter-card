#!/usr/bin/python
import paramiko
import sched
import time
import matplotlib.pyplot as plt

startTime = time.time()
current1Y = []
timeX = []
current2Y = []
shunt1Y = []
shunt2Y = []
bus1Y = []
bus2Y = []

plt.ion()
plt.figure(1)

s1 = paramiko.SSHClient()
s1.set_missing_host_key_policy(paramiko.AutoAddPolicy())
s1.connect('192.168.50.118', 22, 'pi', 'raspberry')

s2 = paramiko.SSHClient()
s2.set_missing_host_key_policy(paramiko.AutoAddPolicy())
s2.connect('192.168.50.119', 22, 'pi', 'raspberry')

def periodic(scheduler, interval, action, actionargs=()):
    scheduler.enter(interval, 1, periodic, (scheduler, interval, action, actionargs))
    action(*actionargs)

def do_it():
    command = 'python /home/pi/git/daughter-card/measure.py'
    i = 0
    j = 0

    timeX.append(time.time()-startTime)
    print "*****************************"

    print "Node 1 Measurements:"
    (stdin, stdout, stderr) = s1.exec_command(command)
    for line in stdout.readlines():

        i = i + 1

        if i == 1:
            print "  Shunt Voltage:"
            shunt1Y.append(float(line))
        elif i == 2:
            print "  Bus Voltage:"
            bus1Y.append(float(line))
        elif i == 3:
            print "  Current:"
            current1Y.append(float(line))

        print "    " + line.rstrip('\n')

    print "Node 2 Measurements:"
    (stdin, stdout, stderr) = s2.exec_command(command)
    for line in stdout.readlines():

        j = j + 1

        if j == 1:
            print "  Shunt Voltage:"
            shunt2Y.append(float(line))
        elif j == 2:
            print "  Bus Voltage:"
            bus2Y.append(float(line))
        elif j == 3:
            print "  Current:"
            current2Y.append(float(line))

        print "    " + line.rstrip('\n')

    plt.subplot(321)
    plt.plot(timeX, current1Y)
    plt.xlabel('Time (s)')
    plt.ylabel('Current (mA)')
    plt.title('Node 1 Current')
    plt.subplot(322)
    plt.plot(timeX, current2Y)
    plt.xlabel('Time (s)')
    plt.ylabel('Current (mA)')
    plt.title('Node 1 Current')
    plt.subplot(323)
    plt.plot(timeX, bus1Y)
    plt.xlabel('Time (s)')
    plt.ylabel('Bus Voltage (V)')
    plt.title('Node 1 Bus Voltage')
    plt.subplot(324)
    plt.plot(timeX, bus2Y)
    plt.xlabel('Time (s)')
    plt.ylabel('Bus Voltage (V)')
    plt.title('Node 2 Bus Voltage')
    plt.subplot(325)
    plt.plot(timeX, shunt1Y)
    plt.xlabel('Time (s)')
    plt.ylabel('Shunt Voltage (mV)')
    plt.title('Node 2 Shunt Voltage')
    plt.subplot(326)
    plt.plot(timeX, shunt2Y)
    plt.xlabel('Time (s)')
    plt.ylabel('Shunt Voltage (mV)')
    plt.title('Node 2 Shunt Voltage')
    plt.draw()
    plt.tight_layout()

scheduler = sched.scheduler(time.time, time.sleep)
periodic(scheduler, 1, do_it)
scheduler.run()

plt.close()
s1.close()
s2.close()
