#!/usr/bin/python

from Subfact_ina219 import INA219

ina = INA219()
result = ina.getBusVoltage_V()

print "%.3f" % ina.getShuntVoltage_mV()
print "%.3f" % ina.getBusVoltage_V()
print "%.3f" % ina.getCurrent_mA()
